package com.example.rafiquee.mindcoach_adminandtherapist.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.Retrofit.CustomCallback;
import com.example.rafiquee.mindcoach_adminandtherapist.Retrofit.RetrofitJSONResponse;
import com.example.rafiquee.mindcoach_adminandtherapist.Retrofit.WebServicesHandler;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.AppointmentsListFragment;
import com.example.rafiquee.mindcoach_adminandtherapist.models.AppointmentItem;
import com.example.rafiquee.mindcoach_adminandtherapist.models.RequestNotificaton;
import com.example.rafiquee.mindcoach_adminandtherapist.models.SendNotificationModel;
import com.example.rafiquee.mindcoach_adminandtherapist.utils.ConstantIds;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("ValidFragment")
public class AppointmentDetailsActivity extends BaseActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_appt_duration)
    TextView tvApptDuration;
    @BindView(R.id.tv_appt_date)
    TextView tvApptDate;
    @BindView(R.id.tv_phone_num)
    TextView tvPhoneNum;

    private AppointmentItem appointmentItem;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_details);

        ButterKnife.bind(this);
        initializations();
        setValuesOnViews();
    }

    private void initializations() {
        setTitle("Appointment Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        if (getIntent().hasExtra("appointment_item"))
            appointmentItem = getIntent().getParcelableExtra("appointment_item");
    }

    @OnClick(R.id.btn_cancel_appointment)
    void onCancelApptClick() {
        if (appointmentItem.getStatus().equals(ConstantIds.CANCELLED))
            Toast.makeText(this, "This appointment is cancelled already!", Toast.LENGTH_SHORT).show();
        else
            openWarningSweetDialog();
    }

    @SuppressLint("SetTextI18n")
    private void setValuesOnViews() {
        if (appointmentItem != null) {
            tvName.setText(appointmentItem.getPatientName());
            tvAddress.setText(appointmentItem.getTherapistAddress());
            tvApptDuration.setText(getFormattedTime(appointmentItem.getTimeFrom()) + " - " +
                    getFormattedTime(appointmentItem.getTimeTo()));
            tvApptDate.setText(getFormattedDate(appointmentItem.getDate()));
            tvPhoneNum.setText(appointmentItem.getPatientPhone());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private String getFormattedTime(long time) {
        Date timeFromDate = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private String getFormattedDate(long time) {
        Date timeFromDate = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private void openWarningSweetDialog() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You would not be able to change status after cancelling it!")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        databaseReference.child("appointments_list").child(appointmentItem.getApptId())
                                .child("status").setValue(ConstantIds.CANCELLED);

                        appointmentItem.setStatus(ConstantIds.CANCELLED);
                        AppointmentsListFragment.APPT_STATUS_UPDATED = true;
                        notifyPatientWIthNotification();

                    }
                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismissWithAnimation();
            }
        }).show();
    }

    private void notifyPatientWIthNotification() {
        RequestNotificaton requestNotificaton = new RequestNotificaton();
        requestNotificaton.setToken("/topics/" + appointmentItem.getPatientId());
        requestNotificaton.setSendNotificationModel(new SendNotificationModel("Appointment Cancelled!",
                appointmentItem.getTherapistName() + " has cancelled the appointment at " +
                        getFormattedTime(appointmentItem.getTimeFrom()) + ", " + getFormattedDate(appointmentItem.getDate())));

        WebServicesHandler.instance.sendNotification(requestNotificaton, new CustomCallback<RetrofitJSONResponse>() {
            @Override
            public void onSuccess(@NonNull RetrofitJSONResponse response) throws Exception {

            }

            @Override
            public void onFailure(boolean completed, @Nullable RetrofitJSONResponse response, @Nullable Exception ex) throws JSONException {

            }
        });
    }
}
