package com.example.rafiquee.mindcoach_adminandtherapist.view_holders;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.AppointmentItem;
import com.example.rafiquee.mindcoach_adminandtherapist.models.PatientInfo;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PatientsListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_patient_pic)
    public ImageView ivPatientPic;
    @BindView(R.id.tv_name)
    public TextView tvName;
    @BindView(R.id.tv_phone_num)
    public TextView tvPhoneNum;
    @BindView(R.id.tv_country)
    public TextView tvCountry;
    @BindView(R.id.tv_under_treatment)
    public TextView tvUnderTreatment;

    private ArrayList<PatientInfo> patientsList;
    private CustomOnClick customOnClick;

    public PatientsListViewHolder(final View itemView, ArrayList<PatientInfo> patientsList, final CustomOnClick customOnClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.patientsList = patientsList;
        this.customOnClick = customOnClick;

        itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                customOnClick.onClick(itemView, getAdapterPosition());
            }
        });
    }
}

