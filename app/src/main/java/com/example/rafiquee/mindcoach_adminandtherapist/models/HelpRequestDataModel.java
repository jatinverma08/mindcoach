package com.example.rafiquee.mindcoach_adminandtherapist.models;

public class HelpRequestDataModel {

    String title, tokenId, patientName, phone, purpose, location, patientLat, patientLng;

    public HelpRequestDataModel(String title, String tokenId, String patientName, String phone, String purpose, String location, String patientLat, String patientLng) {
        this.title = title;
        this.tokenId = tokenId;
        this.patientName = patientName;
        this.phone = phone;
        this.purpose = purpose;
        this.location = location;
        this.patientLat = patientLat;
        this.patientLng = patientLng;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getPatientLat() {
        return patientLat;
    }

    public void setPatientLat(String patientLat) {
        this.patientLat = patientLat;
    }

    public String getPatientLng() {
        return patientLng;
    }

    public void setPatientLng(String patientLng) {
        this.patientLng = patientLng;
    }
}
