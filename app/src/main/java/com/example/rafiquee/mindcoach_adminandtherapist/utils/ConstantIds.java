package com.example.rafiquee.mindcoach_adminandtherapist.utils;

public class ConstantIds {

    public static final String IS_THERAPIST_LOGGED_IN = "IS_THERAPIST_LOGGED_IN";
    public static final String PENDING = "PENDING";
    public static final String CANCELLED = "CANCELLED";
    public static final String EXPIRED = "EXPIRED";
    public static final String HEADER_UPDATE = "HEADER_UPDATE";

    public static final String CAD_INTERNATIONAL_MOBILE_FORMAT_1 = "\\+\\d{2} \\d{3} \\d{7}";
    public static final String CAD_INTERNATIONAL_MOBILE_FORMAT_2 = "\\+\\d{2} \\d{10}";
    public static final String CAD_INTERNATIONAL_MOBILE_FORMAT_3 = "\\+\\d{12}";
    public static final String CAD_NATIONAL_MOBILE_FORMAT_1 = "\\d{4} \\d{7}";
    public static final String CAD_NATIONAL_MOBILE_FORMAT_2 = "\\d{11}";
}
