package com.example.rafiquee.mindcoach_adminandtherapist.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.rafiquee.mindcoach_adminandtherapist.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpecialitiesListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_speciality)
    public TextView tvSpeciality;


    private ArrayList<String> specialitiesList;

    public SpecialitiesListViewHolder(final View itemView, ArrayList<String> specialitiesList) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.specialitiesList = specialitiesList;
    }
}

