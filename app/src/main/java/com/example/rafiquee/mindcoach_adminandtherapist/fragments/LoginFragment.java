package com.example.rafiquee.mindcoach_adminandtherapist.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.activities.AdminPanelActivity;
import com.example.rafiquee.mindcoach_adminandtherapist.activities.HomeActivity;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfo;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfoORM;
import com.example.rafiquee.mindcoach_adminandtherapist.shared_prefs.SharedPreferencesUtility;
import com.example.rafiquee.mindcoach_adminandtherapist.utils.ConstantIds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;

    private TherapistInfo therapistInfo;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    private static final int REQUEST_CODE_ALL_PERMISSION = 2323;

    @SuppressLint("ValidFragment")
    public LoginFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        therapistInfo = new TherapistInfo();
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @OnClick(R.id.btn_login)
    void onBtnLoginClick() {
        if (isConnectedToInternet()) {
            if (isValid()) {
                if (isAdmin()) {
                    startActivity(new Intent(getActivity(), AdminPanelActivity.class));
                    getActivity().finish();
                } else
                    doLogin();
            }
        } else
            Snackbar.make(getView(), "Internet connection problem!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @OnClick(R.id.tv_forgot_password)
    void onTvForgotPasswordClick() {
        if (!TextUtils.isEmpty(etEmail.getText().toString())) {
            validateEmail();
        } else {
            Snackbar.make(getView(), "Please enter email first!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            Snackbar.make(getView(), "Please enter email!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
            Snackbar.make(getView(), "Please enter password!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        }

        return true;
    }

    private boolean isAdmin() {
        return TextUtils.equals(etEmail.getText().toString(), "admin@gmail.com")
                && TextUtils.equals(etPassword.getText().toString(), "admin123");
    }


    private void doLogin() {

        progressDialog = ProgressDialog.show(getActivity(), "", "Logging in...");

        databaseReference.child("therapists_list").orderByChild("email").equalTo(etEmail.getText().toString())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot data : dataSnapshot.getChildren())
                                therapistInfo = data.getValue(TherapistInfo.class);

                            if (TextUtils.equals(etPassword.getText().toString(), therapistInfo.getPassword())) {
                                Toast.makeText(getActivity(), "User successfully logged in!", Toast.LENGTH_SHORT).show();
                                setupLocalDB();
                                getAllPermissions();
                            } else {
                                Snackbar.make(getView(), "Password is incorrect!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            }
                        } else {
                            Snackbar.make(getView(), "This email does not exists!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    private void setupLocalDB() {
                        SharedPreferencesUtility.setPreference(getActivity(), ConstantIds.IS_THERAPIST_LOGGED_IN, true);

                        TherapistInfoORM therapistInfoORM = TherapistInfoORM.findById(TherapistInfoORM.class, 1L);
                        if (therapistInfoORM == null || !therapistInfoORM.getEmail().equals(etEmail.getText().toString())) {
                            if (therapistInfoORM == null)
                                therapistInfoORM = new TherapistInfoORM();
                            therapistInfoORM.setTherapistId(therapistInfo.getTherapistId());
                            therapistInfoORM.setName(therapistInfo.getName());
                            therapistInfoORM.setGender(therapistInfo.getGender());
                            therapistInfoORM.setEmail(therapistInfo.getEmail());
                            therapistInfoORM.setPassword(therapistInfo.getPassword());
                            therapistInfoORM.setDegrees(therapistInfo.getDegrees());
                            therapistInfoORM.setAddress(therapistInfo.getAddress());
                            therapistInfoORM.setPhoneNum(therapistInfo.getPhoneNum());
                            therapistInfoORM.setPicUrl(therapistInfo.getPicUrl());
                            therapistInfoORM.setAvailableDays(therapistInfo.getAvailableDays());
                            therapistInfoORM.setAvailableTimeFrom(therapistInfo.getAvailableTimeFrom());
                            therapistInfoORM.setAvailableTimeTo(therapistInfo.getAvailableTimeTo());
                            therapistInfoORM.setSpecialitiesList(therapistInfo.getSpecialitiesList());
                            therapistInfoORM.setPatientsList(therapistInfo.getPatientsList());
                            therapistInfoORM.save();
                        }
                    }
                });
    }

    private void validateEmail() {

        progressDialog = ProgressDialog.show(getActivity(), "Please wait...", "");

        databaseReference.child("therapists_list").orderByChild("email").equalTo(etEmail.getText().toString())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            TherapistInfoORM therapistInfoORM = null;
                            for (DataSnapshot data : dataSnapshot.getChildren())
                                therapistInfoORM= data.getValue(TherapistInfoORM.class);

                            replaceFragment(new ForgotPasswordFragment(therapistInfoORM), null,true);
                        } else {
                            Snackbar.make(getView(), "This email is not registered!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void getAllPermissions() {
        String[] PERMISSIONS = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                android.Manifest.permission.SEND_SMS,
        };

        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            requestPermissions(PERMISSIONS, REQUEST_CODE_ALL_PERMISSION);
        } else {
            startActivity(new Intent(getActivity(), HomeActivity.class));
            getActivity().finish();
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ALL_PERMISSION:
                for (int permission : grantResults) {
                    if (permission == PackageManager.PERMISSION_DENIED) {
                        return;
                    }
                }

                startActivity(new Intent(getActivity(), HomeActivity.class));
                getActivity().finish();
                break;
        }
    }
}