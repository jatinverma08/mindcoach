package com.example.rafiquee.mindcoach_adminandtherapist.activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.LoginFragment;
import com.example.rafiquee.mindcoach_adminandtherapist.shared_prefs.SharedPreferencesUtility;
import com.example.rafiquee.mindcoach_adminandtherapist.utils.ConstantIds;
import com.google.firebase.FirebaseApp;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseApp.initializeApp(this);


        //______ don't show login page again if already logged in _______
        if (SharedPreferencesUtility.getPreference(this, ConstantIds.IS_THERAPIST_LOGGED_IN, false)) {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        } else
            replaceFragment(new LoginFragment(), null, false);
    }
}
