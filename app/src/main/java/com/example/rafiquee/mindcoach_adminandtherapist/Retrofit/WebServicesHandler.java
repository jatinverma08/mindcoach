package com.example.rafiquee.mindcoach_adminandtherapist.Retrofit;


import android.util.Log;

import com.example.rafiquee.mindcoach_adminandtherapist.models.HelpRequest;
import com.example.rafiquee.mindcoach_adminandtherapist.models.RequestNotificaton;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;

//  Copyright © 2017 AirFive. All rights reserved.

public class WebServicesHandler {

    public static final String BASE_URL = "https://fcm.googleapis.com/";
    public static WebServicesHandler instance = new WebServicesHandler();
    private WebServices webServices;

    private WebServicesHandler() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(120, TimeUnit.SECONDS);
        httpClient.connectTimeout(120, TimeUnit.SECONDS);
        httpClient.writeTimeout(120, TimeUnit.SECONDS);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(RetrofitGSONConverter.create())
                .client(httpClient.build());
//                .client(getUnsafeOkHttpClient());//to get secured urls

        Retrofit retrofit = builder.build();
        webServices = retrofit.create(WebServices.class);

    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            Log.d("-------- authtype", authType);
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            Log.d("-------- authtype", authType);
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void sendNotification(RequestNotificaton requestNotificaton, CustomCallback<RetrofitJSONResponse> callback) {

        Call<RetrofitJSONResponse> call = webServices.sendNotification(requestNotificaton);
        call.enqueue(callback);

    }

    public void sendNotificationOnSubscribedTopic(HelpRequest helpRequest, CustomCallback<RetrofitJSONResponse> callback) {
        Call<RetrofitJSONResponse> call = webServices.sendNotificationOnSubscribedTopic(helpRequest);
        call.enqueue(callback);
    }
}