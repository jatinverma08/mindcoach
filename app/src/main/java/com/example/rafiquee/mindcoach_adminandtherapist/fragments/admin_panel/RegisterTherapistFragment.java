package com.example.rafiquee.mindcoach_adminandtherapist.fragments.admin_panel;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;
import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.adapters.ApptSlotsAdapter;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.BaseFragment;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.ApptSlotItem;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfo;
import com.example.rafiquee.mindcoach_adminandtherapist.utils.ConstantIds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.app.Activity.RESULT_OK;

public class RegisterTherapistFragment extends BaseFragment {

    @BindView(R.id.iv_therapist_pic)
    ImageView ivTherapistPic;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_phone_num)
    EditText etPhoneNum;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.et_degrees)
    EditText etDegrees;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.sp_specialities)
    MultiSpinnerSearch spSpecialities;
    @BindView(R.id.sp_available_days)
    MultiSpinnerSearch spAvailableDays;
    @BindView(R.id.et_time_from)
    EditText etTimeFrom;
    @BindView(R.id.et_time_to)
    EditText etTimeTo;
    @BindView(R.id.rg_gender)
    RadioGroup rgGender;

    @BindView(R.id.cv_set_slots_timings)
    CardView cvSetSlotsTimings;
    @BindView(R.id.btn_monday)
    Button btnMonday;
    @BindView(R.id.btn_tuesday)
    Button btnTuesday;
    @BindView(R.id.btn_wednesday)
    Button btnWednesday;
    @BindView(R.id.btn_thursday)
    Button btnThursday;
    @BindView(R.id.btn_friday)
    Button btnFriday;
    @BindView(R.id.btn_saturday)
    Button btnSaturday;
    @BindView(R.id.btn_sunday)
    Button btnSunday;

    //______ parent layouts of appointment slots ____
    private View setMondaySlotsLayout, setTuesdaySlotsLayout, setWednesdaySlotsLayout, setThursdaySlotsLayout, setFridaySlotsLayout,
            setSaturdaySlotsLayout, setSundaySlotsLayout;

    private EditText etTimeFrom_monday, etTimeFrom_tuesday, etTimeFrom_wednesday, etTimeFrom_thursday, etTimeFrom_friday, etTimeFrom_saturday,
            etTimeFrom_sunday, etTimeTo_monday, etTimeTo_tuesday, etTimeTo_wednesday, etTimeTo_thursday, etTimeTo_friday,
            etTimeTo_saturday, etTimeTo_sunday;
    private RecyclerView rvMondaySlots, rvTuesdaySlots, rvWednesdaySlots, rvThursdaySlots, rvFridaySlots, rvSaturdaySlots, rvSundaySlots;

    private ArrayList<ApptSlotItem> mondaySlotsList = new ArrayList<>();
    private ArrayList<ApptSlotItem> tuesdaySlotsList = new ArrayList<>();
    private ArrayList<ApptSlotItem> wednesdaySlotsList = new ArrayList<>();
    private ArrayList<ApptSlotItem> thursdaySlotsList = new ArrayList<>();
    private ArrayList<ApptSlotItem> fridaySlotsList = new ArrayList<>();
    private ArrayList<ApptSlotItem> saturdaySlotsList = new ArrayList<>();
    private ArrayList<ApptSlotItem> sundaySlotsList = new ArrayList<>();

    private ApptSlotsAdapter mondaySlotsAdapter, tuesdaySlotsAdapter, wednesdaySlotsAdapter, thursdaySlotsAdapter,
            fridaySlotsAdapter, saturdaySlotsAdapter, sundaySlotsAdapter;
    ApptSlotItem apptSlotItem;

    private TherapistInfo therapistInfo;
    private ProgressDialog progressDialog;
    private Uri selectedImagePathUri;
    private static final int Image_Request_Code = 121;
    private Calendar calendarMonday = Calendar.getInstance();
    private Calendar calendarTuesday = Calendar.getInstance();
    private Calendar calendarWednesday = Calendar.getInstance();
    private Calendar calendarThursday = Calendar.getInstance();
    private Calendar calendarFriday = Calendar.getInstance();
    private Calendar calendarSaturday = Calendar.getInstance();
    private Calendar calendarSunday = Calendar.getInstance();
    private DatabaseReference databaseReference;
    private StorageReference storageReference;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_therapist, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        therapistInfo = new TherapistInfo();

        databaseReference = FirebaseDatabase.getInstance().getReference();
        storageReference = FirebaseStorage.getInstance().getReference();

        initByIds(view);
        setOnClickListener();
        setupAvailableDaysSpinner();
        setupSpecialitiesSpinner();

        setupAdapterOnMondaySlots();
        setupAdapterOnTuesdaySlots();
        setupAdapterOnWednesdaySlots();
        setupAdapterOnThursdaySlots();
        setupAdapterOnFridaySlots();
        setupAdapterOnSaturdaySlots();
        setupAdapterOnSundaySlots();
    }

    @OnClick(R.id.iv_back_arrow)
    void onBackArrowClick() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @OnClick(R.id.iv_therapist_pic)
    void onIvTherapistPicClick() {
        chooseProfilePictureFromStorage();
    }

    @OnClick(R.id.btn_pick_specialities)
    void onBtnPickSpecialtiesClick() {
        spSpecialities.performClick();
    }

    @OnClick(R.id.btn_pick_days)
    void onBtnPickDaysClick() {
        spAvailableDays.performClick();
    }

    @OnClick(R.id.btn_register_therapist)
    void onBtnRegisterClick() {
        if (isConnectedToInternet()) {
            if (isValid()) {
                doRegister();
            }
        }else
            Toast.makeText(getActivity(), "Internet connection problem!", Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.rb_male, R.id.rb_female})
    void onRgGenderClicked(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()) {

            case R.id.rb_male:
                therapistInfo.setGender("Male");
                break;

            case R.id.rb_female:
                therapistInfo.setGender("Female");
                break;
        }
    }

    @OnClick(R.id.btn_monday)
    void onBtnMondayClick() {
        hideOrViewSlotsTimingsLayouts("Monday");
    }

    @OnClick(R.id.btn_tuesday)
    void onBtnTuesdayClick() {
        hideOrViewSlotsTimingsLayouts("Tuesday");
    }

    @OnClick(R.id.btn_wednesday)
    void onBtnWednesdayClick() {
        hideOrViewSlotsTimingsLayouts("Wednesday");
    }

    @OnClick(R.id.btn_thursday)
    void onBtnThursdayClick() {
        hideOrViewSlotsTimingsLayouts("Thursday");
    }

    @OnClick(R.id.btn_friday)
    void onBtnFridayClick() {
        hideOrViewSlotsTimingsLayouts("Friday");
    }

    @OnClick(R.id.btn_saturday)
    void onBtnSaturdayClick() {
        hideOrViewSlotsTimingsLayouts("Saturday");
    }

    @OnClick(R.id.btn_sunday)
    void onBtnSundayClick() {
        hideOrViewSlotsTimingsLayouts("Sunday");
    }

    private void setOnClickListener() {
        etTimeFrom_monday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeFrom_monday, calendarMonday.get(Calendar.HOUR_OF_DAY),
                        calendarMonday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeFrom_tuesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeFrom_tuesday, calendarTuesday.get(Calendar.HOUR_OF_DAY),
                        calendarTuesday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeFrom_wednesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeFrom_wednesday, calendarWednesday.get(Calendar.HOUR_OF_DAY),
                        calendarWednesday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeFrom_thursday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeFrom_thursday, calendarThursday.get(Calendar.HOUR_OF_DAY),
                        calendarThursday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeFrom_friday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeFrom_friday, calendarFriday.get(Calendar.HOUR_OF_DAY),
                        calendarFriday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeFrom_saturday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeFrom_saturday, calendarSaturday.get(Calendar.HOUR_OF_DAY),
                        calendarSaturday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeFrom_sunday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeFrom_sunday, calendarSunday.get(Calendar.HOUR_OF_DAY),
                        calendarSunday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeTo_monday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeTo_monday, calendarMonday.get(Calendar.HOUR_OF_DAY),
                        calendarMonday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeTo_tuesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeTo_tuesday, calendarTuesday.get(Calendar.HOUR_OF_DAY),
                        calendarTuesday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeTo_wednesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeTo_wednesday, calendarWednesday.get(Calendar.HOUR_OF_DAY),
                        calendarWednesday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeTo_thursday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeTo_thursday, calendarThursday.get(Calendar.HOUR_OF_DAY),
                        calendarThursday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeTo_friday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeTo_friday, calendarFriday.get(Calendar.HOUR_OF_DAY),
                        calendarFriday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeTo_saturday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeTo_saturday, calendarSaturday.get(Calendar.HOUR_OF_DAY),
                        calendarSaturday.get(Calendar.MINUTE), false).show();
            }
        });

        etTimeTo_sunday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), tpTimeTo_sunday, calendarSunday.get(Calendar.HOUR_OF_DAY),
                        calendarSunday.get(Calendar.MINUTE), false).show();
            }
        });

    }

    private void hideOrViewSlotsTimingsLayouts(String dayToVisible) {
        switch (dayToVisible) {

            case "Monday":
                setMondaySlotsLayout.setVisibility(View.VISIBLE);
                setTuesdaySlotsLayout.setVisibility(View.GONE);
                setWednesdaySlotsLayout.setVisibility(View.GONE);
                setThursdaySlotsLayout.setVisibility(View.GONE);
                setFridaySlotsLayout.setVisibility(View.GONE);
                setSaturdaySlotsLayout.setVisibility(View.GONE);
                setSundaySlotsLayout.setVisibility(View.GONE);
                break;

            case "Tuesday":
                setMondaySlotsLayout.setVisibility(View.GONE);
                setTuesdaySlotsLayout.setVisibility(View.VISIBLE);
                setWednesdaySlotsLayout.setVisibility(View.GONE);
                setThursdaySlotsLayout.setVisibility(View.GONE);
                setFridaySlotsLayout.setVisibility(View.GONE);
                setSaturdaySlotsLayout.setVisibility(View.GONE);
                setSundaySlotsLayout.setVisibility(View.GONE);
                break;

            case "Wednesday":
                setMondaySlotsLayout.setVisibility(View.GONE);
                setTuesdaySlotsLayout.setVisibility(View.GONE);
                setWednesdaySlotsLayout.setVisibility(View.VISIBLE);
                setThursdaySlotsLayout.setVisibility(View.GONE);
                setFridaySlotsLayout.setVisibility(View.GONE);
                setSaturdaySlotsLayout.setVisibility(View.GONE);
                setSundaySlotsLayout.setVisibility(View.GONE);
                break;

            case "Thursday":
                setMondaySlotsLayout.setVisibility(View.GONE);
                setTuesdaySlotsLayout.setVisibility(View.GONE);
                setWednesdaySlotsLayout.setVisibility(View.GONE);
                setThursdaySlotsLayout.setVisibility(View.VISIBLE);
                setFridaySlotsLayout.setVisibility(View.GONE);
                setSaturdaySlotsLayout.setVisibility(View.GONE);
                setSundaySlotsLayout.setVisibility(View.GONE);
                break;

            case "Friday":
                setMondaySlotsLayout.setVisibility(View.GONE);
                setTuesdaySlotsLayout.setVisibility(View.GONE);
                setWednesdaySlotsLayout.setVisibility(View.GONE);
                setThursdaySlotsLayout.setVisibility(View.GONE);
                setFridaySlotsLayout.setVisibility(View.VISIBLE);
                setSaturdaySlotsLayout.setVisibility(View.GONE);
                setSundaySlotsLayout.setVisibility(View.GONE);
                break;

            case "Saturday":
                setMondaySlotsLayout.setVisibility(View.GONE);
                setTuesdaySlotsLayout.setVisibility(View.GONE);
                setWednesdaySlotsLayout.setVisibility(View.GONE);
                setThursdaySlotsLayout.setVisibility(View.GONE);
                setFridaySlotsLayout.setVisibility(View.GONE);
                setSaturdaySlotsLayout.setVisibility(View.VISIBLE);
                setSundaySlotsLayout.setVisibility(View.GONE);
                break;

            case "Sunday":
                setMondaySlotsLayout.setVisibility(View.GONE);
                setTuesdaySlotsLayout.setVisibility(View.GONE);
                setWednesdaySlotsLayout.setVisibility(View.GONE);
                setThursdaySlotsLayout.setVisibility(View.GONE);
                setFridaySlotsLayout.setVisibility(View.GONE);
                setSaturdaySlotsLayout.setVisibility(View.GONE);
                setSundaySlotsLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    private boolean isValid() {

        if (TextUtils.isEmpty(etName.getText().toString())) {
            Snackbar.make(getView(), "Please enter name!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
            Snackbar.make(getView(), "Please enter email!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etPhoneNum.getText().toString())) {
            Snackbar.make(getView(), "Please enter phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etAddress.getText().toString())) {
            Snackbar.make(getView(), "Please enter address!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etDegrees.getText().toString())) {
            Snackbar.make(getView(), "Please enter degrees!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
            Snackbar.make(getView(), "Please enter password!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (TextUtils.isEmpty(etConfirmPassword.getText().toString())) {
            Snackbar.make(getView(), "Please enter password!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (spSpecialities.getSelectedItems().size() == 0) {
            Snackbar.make(getView(), "Please enter specialities!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        } else if (spAvailableDays.getSelectedItems().size() == 0) {
            Snackbar.make(getView(), "Please enter available days!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        }

        //____ email validation ______
        if (!TextUtils.isEmpty(etEmail.getText().toString())) {
            if (!etEmail.getText().toString().contains(".com") || !android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
                Snackbar.make(getView(), "Please enter valid email!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
        }

        //____ phone validation ______
//        if (!TextUtils.isEmpty(etPhoneNum.getText().toString())) {
//            String phone = etPhoneNum.getText().toString();
//
//            if (phone.startsWith("64") || phone.startsWith("+1")) {
//                if (phone.startsWith("64") && !phone.matches(ConstantIds.CAD_NATIONAL_MOBILE_FORMAT_1) && !phone.matches(ConstantIds.CAD_NATIONAL_MOBILE_FORMAT_2)) {
//                    Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
//                    return false;
//                } else if (phone.startsWith("+1") && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_1)
//                        && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_2) && !phone.matches(ConstantIds.CAD_INTERNATIONAL_MOBILE_FORMAT_3)) {
//                    Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
//                    return false;
//                }
//            } else {
//                Snackbar.make(getView(), "Please enter valid phone number!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
//                return false;
//            }
//        }

        //____ password validation ______
        if (!TextUtils.isEmpty(etPassword.getText().toString())) {
            String password = etPassword.getText().toString();
            if (password.length() < 6) {
                Snackbar.make(getView(), "Password length should be 6 at least!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
            if (password.matches("^[a-zA-Z]*$") || password.matches("^[0-9]+$")) {
                Snackbar.make(getView(), "Password should contains digits and alphabets!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return false;
            }
        }

        if (!TextUtils.equals(etPassword.getText().toString(), etConfirmPassword.getText().toString())) {
            Snackbar.make(getView(), "Password does not match!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return false;
        }

        return true;
    }

    private void doRegister() {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please wait...");

        //_____________ Query to get the all the data of a specific user w.r.t Email _______
        databaseReference.child("therapists_list").orderByChild("email").equalTo(etEmail.getText().toString())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                        progressDialog.dismiss();
                        if (dataSnapshot.exists())
                            Snackbar.make(getView(), "This email is already in use!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        else
                            uploadImageFileToFirebaseStorage();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (getActivity() != null) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void uploadImageFileToFirebaseStorage() {

        // Checking whether selectedImagePathUri Is empty or not.
        if (selectedImagePathUri != null) {

            progressDialog = ProgressDialog.show(getActivity(), "", "Picture is uploading...");

            StorageReference storageReference2nd = storageReference.child("mind_coach_pictures").child(System.currentTimeMillis() + "." + GetFileExtension(selectedImagePathUri));

            // Adding addOnSuccessListener to second StorageReference.
            storageReference2nd.putFile(selectedImagePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            if (taskSnapshot.getMetadata() != null) {

                                if (taskSnapshot.getMetadata().getReference() != null) {
                                    Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                                    result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri downloadUri) {
                                            progressDialog.dismiss();
                                            registerProcessFunc(downloadUri.toString());
                                        }
                                    });
                                }
                            }
                        }
                    })
                    // If something goes wrong .
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })

                    // On progress change upload time.
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            Snackbar.make(getView(), "Please select picture first!", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

    private void registerProcessFunc(final String downloadUrl) {
        therapistInfo.setPicUrl(downloadUrl);
        therapistInfo.setName(etName.getText().toString());
        therapistInfo.setEmail(etEmail.getText().toString());
        therapistInfo.setPhoneNum(etPhoneNum.getText().toString());
        therapistInfo.setAddress(etAddress.getText().toString());
        therapistInfo.setPassword(etPassword.getText().toString());
        therapistInfo.setDegrees(etDegrees.getText().toString());

        String id = databaseReference.child("therapists_list").push().getKey();
        therapistInfo.setTherapistId(id);
        databaseReference.child("therapists_list").child(id).setValue(therapistInfo);
        databaseReference.child("appointments_slots").child(id).child("monday").setValue(mondaySlotsList);
        databaseReference.child("appointments_slots").child(id).child("tuesday").setValue(tuesdaySlotsList);
        databaseReference.child("appointments_slots").child(id).child("wednesday").setValue(wednesdaySlotsList);
        databaseReference.child("appointments_slots").child(id).child("thursday").setValue(thursdaySlotsList);
        databaseReference.child("appointments_slots").child(id).child("friday").setValue(fridaySlotsList);
        databaseReference.child("appointments_slots").child(id).child("saturday").setValue(saturdaySlotsList);
        databaseReference.child("appointments_slots").child(id).child("sunday").setValue(sundaySlotsList);
        openSweetDialog();
    }

    private void openSweetDialog() {
        new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Registration Completed!")
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                })
                .show();
    }

    private void chooseProfilePictureFromStorage() {

        Intent intent = new Intent();

        // Setting intent type as image to select image from phone storage.
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Please Select Image"), Image_Request_Code);
    }

    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getActivity().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        // Returning the file Extension.
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            selectedImagePathUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImagePathUri);
                ivTherapistPic.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setupAvailableDaysSpinner() {
        List<KeyPairBoolData> daysList = getDaysList(Arrays.asList(getResources().getStringArray(R.array.days_list)));
        spAvailableDays.setItems(daysList, -1, new SpinnerListener() {

            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                ArrayList<String> selectedDaysList = new ArrayList<>();
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        selectedDaysList.add(items.get(i).getName());
                    }
                }

                therapistInfo.setAvailableDays(selectedDaysList);
            }
        });

        spAvailableDays.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cvSetSlotsTimings.setVisibility(View.VISIBLE);
                hideAllDaysButtons();
                for (int i = 0; i < therapistInfo.getAvailableDays().size(); i++) {
                    switchForDaysVisibility(therapistInfo.getAvailableDays().get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void hideAllDaysButtons() {
        btnMonday.setVisibility(View.GONE);
        btnTuesday.setVisibility(View.GONE);
        btnWednesday.setVisibility(View.GONE);
        btnThursday.setVisibility(View.GONE);
        btnFriday.setVisibility(View.GONE);
        btnSaturday.setVisibility(View.GONE);
        btnSunday.setVisibility(View.GONE);
    }

    private void switchForDaysVisibility(String dayName) {
        switch (dayName) {

            case "Monday":
                btnMonday.setVisibility(View.VISIBLE);
                break;

            case "Tuesday":
                btnTuesday.setVisibility(View.VISIBLE);
                break;

            case "Wednesday":
                btnWednesday.setVisibility(View.VISIBLE);
                break;

            case "Thursday":
                btnThursday.setVisibility(View.VISIBLE);
                break;

            case "Friday":
                btnFriday.setVisibility(View.VISIBLE);
                break;

            case "Saturday":
                btnSaturday.setVisibility(View.VISIBLE);
                break;

            case "Sunday":
                btnSunday.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setupSpecialitiesSpinner() {
        List<KeyPairBoolData> specialitiesList = getSpecialitiesList(Arrays.asList(getResources().getStringArray(R.array.specialities_list)));
        spSpecialities.setItems(specialitiesList, -1, new SpinnerListener() {

            @Override
            public void onItemsSelected(List<KeyPairBoolData> items) {
                ArrayList<String> selectedSpecialitiesList = new ArrayList<>();
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).isSelected()) {
                        selectedSpecialitiesList.add(items.get(i).getName());
                    }
                }

                therapistInfo.setSpecialitiesList(selectedSpecialitiesList);
            }
        });
    }

    private void setupAdapterOnMondaySlots() {
        rvMondaySlots.setLayoutManager(new LinearLayoutManager(getActivity()));
        mondaySlotsAdapter = new ApptSlotsAdapter(getActivity(), mondaySlotsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                openConfirmationSweetDialog(mondaySlotsList, position, "Monday");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvMondaySlots.setAdapter(mondaySlotsAdapter);
    }

    private void setupAdapterOnTuesdaySlots() {
        rvTuesdaySlots.setLayoutManager(new LinearLayoutManager(getActivity()));
        tuesdaySlotsAdapter = new ApptSlotsAdapter(getActivity(), tuesdaySlotsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                openConfirmationSweetDialog(tuesdaySlotsList, position, "Tuesday");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvTuesdaySlots.setAdapter(tuesdaySlotsAdapter);
    }

    private void setupAdapterOnWednesdaySlots() {
        rvWednesdaySlots.setLayoutManager(new LinearLayoutManager(getActivity()));
        wednesdaySlotsAdapter = new ApptSlotsAdapter(getActivity(), wednesdaySlotsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                openConfirmationSweetDialog(wednesdaySlotsList, position, "Wednesday");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvWednesdaySlots.setAdapter(wednesdaySlotsAdapter);
    }

    private void setupAdapterOnThursdaySlots() {
        rvThursdaySlots.setLayoutManager(new LinearLayoutManager(getActivity()));
        thursdaySlotsAdapter = new ApptSlotsAdapter(getActivity(), thursdaySlotsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                openConfirmationSweetDialog(thursdaySlotsList, position, "Thursday");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvThursdaySlots.setAdapter(thursdaySlotsAdapter);
    }

    private void setupAdapterOnFridaySlots() {
        rvFridaySlots.setLayoutManager(new LinearLayoutManager(getActivity()));
        fridaySlotsAdapter = new ApptSlotsAdapter(getActivity(), fridaySlotsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                openConfirmationSweetDialog(fridaySlotsList, position, "Friday");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvFridaySlots.setAdapter(fridaySlotsAdapter);
    }

    private void setupAdapterOnSaturdaySlots() {
        rvSaturdaySlots.setLayoutManager(new LinearLayoutManager(getActivity()));
        saturdaySlotsAdapter = new ApptSlotsAdapter(getActivity(), saturdaySlotsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                openConfirmationSweetDialog(saturdaySlotsList, position, "Saturday");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvSaturdaySlots.setAdapter(saturdaySlotsAdapter);
    }

    private void setupAdapterOnSundaySlots() {
        rvSundaySlots.setLayoutManager(new LinearLayoutManager(getActivity()));
        sundaySlotsAdapter = new ApptSlotsAdapter(getActivity(), sundaySlotsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                openConfirmationSweetDialog(sundaySlotsList, position, "Sunday");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvSundaySlots.setAdapter(sundaySlotsAdapter);
    }

    private void openConfirmationSweetDialog(ArrayList<ApptSlotItem> slotsList, final int position, final String day) {
        new SweetAlertDialog(getContext(), SweetAlertDialog.BUTTON_CONFIRM)
                .setTitleText("Do you want to delete this slot?")
                .setConfirmText("Yes")
                .setCancelText("No")
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        switch (day) {

                            case "Monday":
                                mondaySlotsList.remove(position);
                                mondaySlotsAdapter.notifyItemRemoved(position);
                                break;

                            case "Tuesday":
                                tuesdaySlotsList.remove(position);
                                tuesdaySlotsAdapter.notifyItemRemoved(position);
                                break;

                            case "Wednesday":
                                wednesdaySlotsList.remove(position);
                                wednesdaySlotsAdapter.notifyItemRemoved(position);
                                break;

                            case "Thursday":
                                thursdaySlotsList.remove(position);
                                thursdaySlotsAdapter.notifyItemRemoved(position);
                                break;

                            case "Friday":
                                fridaySlotsList.remove(position);
                                fridaySlotsAdapter.notifyItemRemoved(position);
                                break;

                            case "Saturday":
                                saturdaySlotsList.remove(position);
                                saturdaySlotsAdapter.notifyItemRemoved(position);
                                break;

                            case "Sunday":
                                sundaySlotsList.remove(position);
                                sundaySlotsAdapter.notifyItemRemoved(position);
                                break;
                        }
                    }
                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.cancel();
            }
        }).show();
    }

    private List<KeyPairBoolData> getDaysList(List<String> list) {
        final List<KeyPairBoolData> listArray0 = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i + 1);
            h.setName(list.get(i));
            h.setSelected(false);
            listArray0.add(h);
        }

        return listArray0;
    }

    private List<KeyPairBoolData> getSpecialitiesList(List<String> list) {
        final List<KeyPairBoolData> listArray0 = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            KeyPairBoolData h = new KeyPairBoolData();
            h.setId(i + 1);
            h.setName(list.get(i));
            h.setSelected(false);
            listArray0.add(h);
        }

        return listArray0;
    }

    private void initByIds(View view) {
        setMondaySlotsLayout = view.findViewById(R.id.ll_monday_slots_timings);
        setTuesdaySlotsLayout = view.findViewById(R.id.ll_tuesday_slots_timings);
        setWednesdaySlotsLayout = view.findViewById(R.id.ll_wednesday_slots_timings);
        setThursdaySlotsLayout = view.findViewById(R.id.ll_thursday_slots_timings);
        setFridaySlotsLayout = view.findViewById(R.id.ll_friday_slots_timings);
        setSaturdaySlotsLayout = view.findViewById(R.id.ll_saturday_slots_timings);
        setSundaySlotsLayout = view.findViewById(R.id.ll_sunday_slots_timings);

        etTimeFrom_monday = setMondaySlotsLayout.findViewById(R.id.et_time_from);
        etTimeFrom_tuesday = setTuesdaySlotsLayout.findViewById(R.id.et_time_from);
        etTimeFrom_wednesday = setWednesdaySlotsLayout.findViewById(R.id.et_time_from);
        etTimeFrom_thursday = setThursdaySlotsLayout.findViewById(R.id.et_time_from);
        etTimeFrom_friday = setFridaySlotsLayout.findViewById(R.id.et_time_from);
        etTimeFrom_saturday = setSaturdaySlotsLayout.findViewById(R.id.et_time_from);
        etTimeFrom_sunday = setSundaySlotsLayout.findViewById(R.id.et_time_from);

        etTimeTo_monday = setMondaySlotsLayout.findViewById(R.id.et_time_to);
        etTimeTo_tuesday = setTuesdaySlotsLayout.findViewById(R.id.et_time_to);
        etTimeTo_wednesday = setWednesdaySlotsLayout.findViewById(R.id.et_time_to);
        etTimeTo_thursday = setThursdaySlotsLayout.findViewById(R.id.et_time_to);
        etTimeTo_friday = setFridaySlotsLayout.findViewById(R.id.et_time_to);
        etTimeTo_saturday = setSaturdaySlotsLayout.findViewById(R.id.et_time_to);
        etTimeTo_sunday = setSundaySlotsLayout.findViewById(R.id.et_time_to);

        rvMondaySlots = setMondaySlotsLayout.findViewById(R.id.rv_slots_timings);
        rvTuesdaySlots = setTuesdaySlotsLayout.findViewById(R.id.rv_slots_timings);
        rvWednesdaySlots = setWednesdaySlotsLayout.findViewById(R.id.rv_slots_timings);
        rvThursdaySlots = setThursdaySlotsLayout.findViewById(R.id.rv_slots_timings);
        rvFridaySlots = setFridaySlotsLayout.findViewById(R.id.rv_slots_timings);
        rvSaturdaySlots = setSaturdaySlotsLayout.findViewById(R.id.rv_slots_timings);
        rvSundaySlots = setSundaySlotsLayout.findViewById(R.id.rv_slots_timings);
    }


    TimePickerDialog.OnTimeSetListener tpTimeFrom_monday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarMonday.set(Calendar.HOUR_OF_DAY, hours);
            calendarMonday.set(Calendar.MINUTE, minutes);

            apptSlotItem = new ApptSlotItem();
            apptSlotItem.setTimeFrom(calendarMonday.getTimeInMillis());
            etTimeFrom_monday.setText(getFormattedTime(calendarMonday.getTimeInMillis()));
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeFrom_tuesday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarTuesday.set(Calendar.HOUR_OF_DAY, hours);
            calendarTuesday.set(Calendar.MINUTE, minutes);

            apptSlotItem = new ApptSlotItem();
            apptSlotItem.setTimeFrom(calendarTuesday.getTimeInMillis());
            etTimeFrom_tuesday.setText(getFormattedTime(calendarTuesday.getTimeInMillis()));
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeFrom_wednesday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarWednesday.set(Calendar.HOUR_OF_DAY, hours);
            calendarWednesday.set(Calendar.MINUTE, minutes);

            apptSlotItem = new ApptSlotItem();
            apptSlotItem.setTimeFrom(calendarWednesday.getTimeInMillis());
            etTimeFrom_wednesday.setText(getFormattedTime(calendarWednesday.getTimeInMillis()));
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeFrom_thursday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarThursday.set(Calendar.HOUR_OF_DAY, hours);
            calendarThursday.set(Calendar.MINUTE, minutes);

            apptSlotItem = new ApptSlotItem();
            apptSlotItem.setTimeFrom(calendarThursday.getTimeInMillis());
            etTimeFrom_thursday.setText(getFormattedTime(calendarThursday.getTimeInMillis()));
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeFrom_friday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarFriday.set(Calendar.HOUR_OF_DAY, hours);
            calendarFriday.set(Calendar.MINUTE, minutes);

            apptSlotItem = new ApptSlotItem();
            apptSlotItem.setTimeFrom(calendarFriday.getTimeInMillis());
            etTimeFrom_friday.setText(getFormattedTime(calendarFriday.getTimeInMillis()));
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeFrom_saturday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarSaturday.set(Calendar.HOUR_OF_DAY, hours);
            calendarSaturday.set(Calendar.MINUTE, minutes);

            apptSlotItem = new ApptSlotItem();
            apptSlotItem.setTimeFrom(calendarSaturday.getTimeInMillis());
            etTimeFrom_saturday.setText(getFormattedTime(calendarSaturday.getTimeInMillis()));
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeFrom_sunday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarSunday.set(Calendar.HOUR_OF_DAY, hours);
            calendarSunday.set(Calendar.MINUTE, minutes);

            apptSlotItem = new ApptSlotItem();
            apptSlotItem.setTimeFrom(calendarSunday.getTimeInMillis());
            etTimeFrom_sunday.setText(getFormattedTime(calendarSunday.getTimeInMillis()));
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeTo_monday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarMonday.set(Calendar.HOUR_OF_DAY, hours);
            calendarMonday.set(Calendar.MINUTE, minutes);

            if (!TextUtils.isEmpty(etTimeFrom_monday.getText().toString())) {
                apptSlotItem.setTimeFrom(apptSlotItem.getTimeFrom());
                apptSlotItem.setTimeTo(calendarMonday.getTimeInMillis());
                etTimeTo_monday.setText(getFormattedTime(calendarMonday.getTimeInMillis()));
                mondaySlotsList.add(apptSlotItem);

                mondaySlotsAdapter.notifyDataSetChanged();
                etTimeFrom_monday.setText("");
                etTimeTo_monday.setText("");
            } else {
                Toast.makeText(getActivity(), "Please enter time from!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeTo_tuesday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarTuesday.set(Calendar.HOUR_OF_DAY, hours);
            calendarTuesday.set(Calendar.MINUTE, minutes);

            if (!TextUtils.isEmpty(etTimeFrom_tuesday.getText().toString())) {
                apptSlotItem.setTimeFrom(apptSlotItem.getTimeFrom());
                apptSlotItem.setTimeTo(calendarTuesday.getTimeInMillis());
                etTimeTo_tuesday.setText(getFormattedTime(calendarTuesday.getTimeInMillis()));
                tuesdaySlotsList.add(apptSlotItem);

                tuesdaySlotsAdapter.notifyDataSetChanged();
                etTimeFrom_tuesday.setText("");
                etTimeTo_tuesday.setText("");
            } else {
                Toast.makeText(getActivity(), "Please enter time from!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeTo_wednesday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarWednesday.set(Calendar.HOUR_OF_DAY, hours);
            calendarWednesday.set(Calendar.MINUTE, minutes);

            if (!TextUtils.isEmpty(etTimeFrom_wednesday.getText().toString())) {
                apptSlotItem.setTimeFrom(apptSlotItem.getTimeFrom());
                apptSlotItem.setTimeTo(calendarWednesday.getTimeInMillis());
                etTimeTo_wednesday.setText(getFormattedTime(calendarWednesday.getTimeInMillis()));
                wednesdaySlotsList.add(apptSlotItem);

                wednesdaySlotsAdapter.notifyDataSetChanged();
                etTimeFrom_wednesday.setText("");
                etTimeTo_wednesday.setText("");
            } else {
                Toast.makeText(getActivity(), "Please enter time from!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeTo_thursday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarThursday.set(Calendar.HOUR_OF_DAY, hours);
            calendarThursday.set(Calendar.MINUTE, minutes);

            if (!TextUtils.isEmpty(etTimeFrom_thursday.getText().toString())) {
                apptSlotItem.setTimeFrom(apptSlotItem.getTimeFrom());
                apptSlotItem.setTimeTo(calendarThursday.getTimeInMillis());
                etTimeTo_thursday.setText(getFormattedTime(calendarThursday.getTimeInMillis()));
                thursdaySlotsList.add(apptSlotItem);

                thursdaySlotsAdapter.notifyDataSetChanged();
                etTimeFrom_thursday.setText("");
                etTimeTo_thursday.setText("");
            } else {
                Toast.makeText(getActivity(), "Please enter time from!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeTo_friday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarFriday.set(Calendar.HOUR_OF_DAY, hours);
            calendarFriday.set(Calendar.MINUTE, minutes);

            if (!TextUtils.isEmpty(etTimeFrom_friday.getText().toString())) {
                apptSlotItem.setTimeFrom(apptSlotItem.getTimeFrom());
                apptSlotItem.setTimeTo(calendarFriday.getTimeInMillis());
                etTimeTo_friday.setText(getFormattedTime(calendarFriday.getTimeInMillis()));
                fridaySlotsList.add(apptSlotItem);

                fridaySlotsAdapter.notifyDataSetChanged();
                etTimeFrom_friday.setText("");
                etTimeTo_friday.setText("");
            } else {
                Toast.makeText(getActivity(), "Please enter time from!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeTo_saturday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarSaturday.set(Calendar.HOUR_OF_DAY, hours);
            calendarSaturday.set(Calendar.MINUTE, minutes);

            if (!TextUtils.isEmpty(etTimeFrom_saturday.getText().toString())) {
                apptSlotItem.setTimeFrom(apptSlotItem.getTimeFrom());
                apptSlotItem.setTimeTo(calendarSaturday.getTimeInMillis());
                etTimeTo_saturday.setText(getFormattedTime(calendarSaturday.getTimeInMillis()));
                saturdaySlotsList.add(apptSlotItem);

                saturdaySlotsAdapter.notifyDataSetChanged();
                etTimeFrom_saturday.setText("");
                etTimeTo_saturday.setText("");
            } else {
                Toast.makeText(getActivity(), "Please enter time from!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    TimePickerDialog.OnTimeSetListener tpTimeTo_sunday = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
            calendarSunday.set(Calendar.HOUR_OF_DAY, hours);
            calendarSunday.set(Calendar.MINUTE, minutes);

            if (!TextUtils.isEmpty(etTimeFrom_sunday.getText().toString())) {
                apptSlotItem.setTimeFrom(apptSlotItem.getTimeFrom());
                apptSlotItem.setTimeTo(calendarSunday.getTimeInMillis());
                etTimeTo_sunday.setText(getFormattedTime(calendarSunday.getTimeInMillis()));
                sundaySlotsList.add(apptSlotItem);

                sundaySlotsAdapter.notifyDataSetChanged();
                etTimeFrom_sunday.setText("");
                etTimeTo_sunday.setText("");
            } else {
                Toast.makeText(getActivity(), "Please enter time from!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private String getFormattedTime(long timeFrom) {
        Date timeFromDate = new Date(timeFrom);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }
}


