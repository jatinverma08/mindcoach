package com.example.rafiquee.mindcoach_adminandtherapist.models;

public class SendNotificationModel {
    private String title, body;

    public SendNotificationModel(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}