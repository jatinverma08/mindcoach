package com.example.rafiquee.mindcoach_adminandtherapist.fragments.admin_panel;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.adapters.TherapistsListAdapter;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.BaseFragment;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.TherapistDetailsFragment;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TherapistsListFragment extends BaseFragment {

    @BindView(R.id.rv_therapist_list)
    RecyclerView rvTherapistsList;

    private TherapistsListAdapter therapistsListAdapter;
    private ArrayList<TherapistInfo> therapistsList = new ArrayList<>();
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_therapist_list, container, false);
        ButterKnife.bind(this, view);
        initializations();
        fetchTherapistListFromFirebase();
        return view;
    }

    private void initializations() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        rvTherapistsList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @OnClick(R.id.iv_back_arrow)
    void onBackArrowClick() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void fetchTherapistListFromFirebase() {

        if (isConnectedToInternet()) {
            progressDialog = ProgressDialog.show(getActivity(), "", "Please wait...");
            databaseReference.child("therapists_list").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot child : dataSnapshot.getChildren())
                            therapistsList.add(child.getValue(TherapistInfo.class));

                        setupAdapter();
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    if (getActivity() != null) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else
            Toast.makeText(getActivity(), "Internet connection problem!", Toast.LENGTH_SHORT).show();
    }

    private void setupAdapter() {
        therapistsListAdapter = new TherapistsListAdapter(getActivity(), therapistsList, new CustomOnClick() {

            @Override
            public void onClick(View view, int position) {
                addFragment(new TherapistDetailsFragment(therapistsList.get(position)), null, true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvTherapistsList.setAdapter(therapistsListAdapter);
    }
}
