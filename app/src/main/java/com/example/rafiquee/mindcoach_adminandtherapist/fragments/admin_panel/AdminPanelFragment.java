package com.example.rafiquee.mindcoach_adminandtherapist.fragments.admin_panel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.activities.MainActivity;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.BaseFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class AdminPanelFragment extends BaseFragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_panel, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btn_register_therapist)
    void onBtnRegisterTherapistClick() {
        replaceFragment(new RegisterTherapistFragment(), null, true);
    }

    @OnClick(R.id.btn_all_patients)
    void onBtnAllPatientsClick() {
        replaceFragment(new PatientsListFragment(), null, true);
    }

    @OnClick(R.id.btn_all_therapists)
    void onBtnAllTherapistsClick() {
        replaceFragment(new TherapistsListFragment(), null, true);
    }


    @OnClick(R.id.iv_logout)
    void doLogout() {

        new SweetAlertDialog(getContext(), SweetAlertDialog.BUTTON_CONFIRM)
                .setContentText("Do you want to logout?")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                }).show();
    }
}
