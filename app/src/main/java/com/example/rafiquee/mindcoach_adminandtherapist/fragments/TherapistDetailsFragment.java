package com.example.rafiquee.mindcoach_adminandtherapist.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.adapters.AvailableDaysAdapter;
import com.example.rafiquee.mindcoach_adminandtherapist.adapters.SpecialitiesListAdapter;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("ValidFragment")
public class TherapistDetailsFragment extends BaseFragment {

    @BindView(R.id.iv_therapist_pic)
    ImageView ivTherapistPic;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_degrees)
    TextView tvDegrees;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_phone_num)
    TextView tvPhone;
    @BindView(R.id.rv_specialities)
    RecyclerView rvSpecialties;
    @BindView(R.id.rv_available_days)
    RecyclerView rvAvailableDays;

    private TherapistInfo therapistInfo;
    private DatabaseReference databaseReference;


    @SuppressLint("ValidFragment")
    public TherapistDetailsFragment(TherapistInfo therapistInfo) {
        this.therapistInfo = therapistInfo;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_therapist_details, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        setValuesOnViews();
        return view;
    }

    private void initializations(View view) {
        databaseReference = FirebaseDatabase.getInstance().getReference();

        setupSpecialitiesListAdapter();
        setupAvailableDaysListAdapter();
    }

    @OnClick(R.id.iv_back_arrow)
    void onBackArrowClick() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void setValuesOnViews() {
        tvName.setText(therapistInfo.getName());
        tvEmail.setText(therapistInfo.getEmail());
        tvDegrees.setText(therapistInfo.getDegrees());
        tvAddress.setText(therapistInfo.getAddress());
        if (!TextUtils.isEmpty(therapistInfo.getPhoneNum()))
            tvPhone.setText(therapistInfo.getPhoneNum());
        setTherapistPicture();
    }

    private void setupSpecialitiesListAdapter() {
        rvSpecialties.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSpecialties.setAdapter(new SpecialitiesListAdapter(therapistInfo.getSpecialitiesList()));
    }

    private void setupAvailableDaysListAdapter() {
        rvAvailableDays.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvAvailableDays.setAdapter(new AvailableDaysAdapter(therapistInfo.getAvailableDays()));
    }

    private void setTherapistPicture() {
        if (TextUtils.isEmpty(therapistInfo.getPicUrl())) {
            ivTherapistPic.setPadding(0, 0, 0, 0);
            ivTherapistPic.setImageResource(R.drawable.blank_image);
        } else {
            Glide.with(getActivity()).load(therapistInfo.getPicUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .override(300, 200))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            ivTherapistPic.setPadding(0, 0, 0, 0);
                            ivTherapistPic.setImageDrawable(resource);
                        }
                    });
        }
    }
}

