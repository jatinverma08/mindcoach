package com.example.rafiquee.mindcoach_adminandtherapist.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfo;
import com.example.rafiquee.mindcoach_adminandtherapist.view_holders.TherapistListViewHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class TherapistsListAdapter extends RecyclerView.Adapter<TherapistListViewHolder> {

    private Context context;
    private CustomOnClick customOnClick;
    private ArrayList<TherapistInfo> therapistsList;

    public TherapistsListAdapter() {
    }

    public TherapistsListAdapter(Context context, ArrayList<TherapistInfo> therapistsList, CustomOnClick customOnClick) {
        this.context = context;
        this.therapistsList = therapistsList;
        this.customOnClick = customOnClick;
    }

    @Override
    public TherapistListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_therapist_list_item, parent, false);

        TherapistListViewHolder vh = new TherapistListViewHolder(view, therapistsList, customOnClick);
        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(TherapistListViewHolder holder, int position) {
        setPictureOnTherapistItem(position, holder);
        holder.tvName.setText(therapistsList.get(position).getName());
        holder.tvDegrees.setText(therapistsList.get(position).getDegrees());
        holder.tvPhoneNum.setText("Phone: " + therapistsList.get(position).getPhoneNum());
        holder.tvAvailableTiming.setText("Available: " + getFormattedTime(therapistsList.get(position).getAvailableTimeFrom()) +
                " - " + getFormattedTime(therapistsList.get(position).getAvailableTimeTo()));
        holder.tvNumOfPatients.setText("Patients: (" + String.valueOf(therapistsList.get(position).getPatientsList().size()) + ")");
    }

    @Override
    public int getItemCount() {
        return therapistsList.size();
    }

    private String getFormattedTime(long timeFrom) {
        Date timeFromDate = new Date(timeFrom);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private void setPictureOnTherapistItem(int position, final TherapistListViewHolder therapistListViewHolder) {
        if (TextUtils.isEmpty(therapistsList.get(position).getPicUrl())) {
            therapistListViewHolder.ivTherapistPic.setPadding(0, 0, 0, 0);
            therapistListViewHolder.ivTherapistPic.setImageResource(R.drawable.blank_image);
        } else {
            Glide.with(context).load(therapistsList.get(position).getPicUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .override(300, 200))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            therapistListViewHolder.ivTherapistPic.setPadding(0, 0, 0, 0);
                            therapistListViewHolder.ivTherapistPic.setImageDrawable(resource);
                        }
                    });
        }
    }
}