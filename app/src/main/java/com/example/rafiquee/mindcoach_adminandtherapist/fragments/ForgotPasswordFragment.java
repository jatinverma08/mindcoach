package com.example.rafiquee.mindcoach_adminandtherapist.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfoORM;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class ForgotPasswordFragment extends BaseFragment {

    private static final int REQUEST_CODE_SMS_PERMISSION = 121;
    @BindView(R.id.et_phone_num)
    EditText etPhone;

    private TherapistInfoORM therapistInfoORM;
    private int code;

    @SuppressLint("ValidFragment")
    public ForgotPasswordFragment(TherapistInfoORM therapistInfoORM) {
        this.therapistInfoORM = therapistInfoORM;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        return view;
    }

    private void initializations(View view) {
        etPhone.setText(therapistInfoORM.getPhoneNum());
    }

    @OnClick(R.id.btn_send)
    void onBtnSendClick() {
        getPermissionsForSMS();
    }

    private void getPermissionsForSMS() {
        String[] PERMISSIONS = {
                android.Manifest.permission.SEND_SMS,
        };

        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, REQUEST_CODE_SMS_PERMISSION);
        } else {
            sendSMSAndOpenDialog(therapistInfoORM.getPassword());
        }
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            sendSMSAndOpenDialog(therapistInfoORM.getPassword());
        }
    }

    public void sendSMSAndOpenDialog(String password) {
        SmsManager smsManager = SmsManager.getDefault();
//        smsManager.sendTextMessage("+923053818344", null, code, null, null);
        smsManager.sendTextMessage(therapistInfoORM.getPhoneNum(), null,
                "Mind Coach\n" + "Password for " + therapistInfoORM.getEmail() + " is : " + password, null, null);
        Toast.makeText(getActivity(), "SMS Sent!", Toast.LENGTH_LONG).show();

        getActivity().getSupportFragmentManager().popBackStack();
    }
}
