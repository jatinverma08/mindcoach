package com.example.rafiquee.mindcoach_adminandtherapist.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.HomeFragment;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.MyAccountFragment;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfoORM;
import com.example.rafiquee.mindcoach_adminandtherapist.shared_prefs.SharedPreferencesUtility;
import com.example.rafiquee.mindcoach_adminandtherapist.utils.ConstantIds;
import com.google.firebase.messaging.FirebaseMessaging;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView ivMyPic;
    private TextView tvName, tvEmail;
    private TherapistInfoORM therapistInfoORM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initializations();
        navigationDrawerSetup();
        setValuesOnViews();
    }


    private void initializations() {
        setTitle("");
        findViewById(R.id.app_bar_layout).bringToFront();//to transparent the drawer actionbar

        therapistInfoORM = TherapistInfoORM.findById(TherapistInfoORM.class, 1L);
        FirebaseMessaging.getInstance().subscribeToTopic(therapistInfoORM.getTherapistId());

        LocalBroadcastManager.getInstance(this).registerReceiver(updateHeaderReceiver, new IntentFilter(ConstantIds.HEADER_UPDATE));

        replaceFragment(new HomeFragment(), null, false);
    }

    private void navigationDrawerSetup() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //______ initialize header views of drawer layout ______
        View headerView = navigationView.getHeaderView(0);
        ivMyPic = headerView.findViewById(R.id.iv_my_pic);
        tvName = headerView.findViewById(R.id.tv_name);
        tvEmail = headerView.findViewById(R.id.tv_email);
    }

    private void setValuesOnViews() {
        setPictureOnImageView();
        tvName.setText(therapistInfoORM.getName());
        tvEmail.setText(therapistInfoORM.getEmail());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main_recording, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.nav_my_account:
                replaceFragment(new MyAccountFragment(), null, true);
                break;

            case R.id.nav_privacy_policy:
                startActivity(new Intent(this, PrivacyPolicyActivity.class));
                break;

            case R.id.nav_share:
                shareTheApp();
                break;

            case R.id.nav_logout:
                SharedPreferencesUtility.setPreference(this, ConstantIds.IS_THERAPIST_LOGGED_IN, false);
                finish();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setPictureOnImageView() {
        if (TextUtils.isEmpty(therapistInfoORM.getPicUrl())) {
            ivMyPic.setPadding(0, 0, 0, 0);
            ivMyPic.setImageResource(R.drawable.blank_image);
        } else {
            Glide.with(this).load(therapistInfoORM.getPicUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .override(300, 200))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            ivMyPic.setPadding(0, 0, 0, 0);
                            ivMyPic.setImageDrawable(resource);
                        }
                    });
        }
    }

    private BroadcastReceiver updateHeaderReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            therapistInfoORM = TherapistInfoORM.findById(TherapistInfoORM.class, 1L);
            setValuesOnViews();
        }
    };

    private void shareTheApp() {

        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Mind Coach");
            i.putExtra(Intent.EXTRA_TEXT, "Mind Coach\nFind the best therapist here. Goto Playstore and download our app.");
            startActivity(Intent.createChooser(i, "Mind Coach"));
        } catch (Exception e) {
            e.toString();
        }
    }
}
