package com.example.rafiquee.mindcoach_adminandtherapist.fragments;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.models.PatientInfo;
import com.example.rafiquee.mindcoach_adminandtherapist.models.Questionnaire;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class UnderTreatmentPatientDetailsFragment extends BaseFragment {

    private static final int Image_Request_Code = 1231;
    @BindView(R.id.iv_my_pic)
    ImageView ivMyPic;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_phone_num)
    TextView tvPhone;
    @BindView(R.id.tv_gender)
    TextView tvGender;
    @BindView(R.id.tv_age)
    TextView tvAge;
    @BindView(R.id.tv_religious)
    TextView tvReligious;
    @BindView(R.id.tv_religion)
    TextView tvReligion;
    @BindView(R.id.tv_has_therapist)
    TextView tvHasTherapist;
    @BindView(R.id.tv_sadness)
    TextView tvSadness;
    @BindView(R.id.tv_suicide_plan)
    TextView tvSuicidePlan;
    @BindView(R.id.tv_panic_attacks)
    TextView tvPanicAttacks;
    @BindView(R.id.tv_taking_medications)
    TextView tvTakingMedications;
    @BindView(R.id.tv_chronic_pain)
    TextView tvChronicPain;
    @BindView(R.id.tv_financial_status)
    TextView tvFinancialStatus;
    @BindView(R.id.tv_sleeping_habits)
    TextView tvSleepingHabits;
    @BindView(R.id.tv_country)
    TextView tvCountry;
    @BindView(R.id.tv_problems)
    TextView tvProblems;


    private PatientInfo patientInfo;

    @SuppressLint("ValidFragment")
    public UnderTreatmentPatientDetailsFragment(PatientInfo patientInfo) {
        this.patientInfo = patientInfo;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_under_treatment_patient_details, container, false);
        ButterKnife.bind(this, view);
        initializations(view);
        setValuesOnViews();
        return view;
    }

    private void initializations(View view) {
    }


    private void setValuesOnViews() {
        setPictureOnImageView();
        tvName.setText(patientInfo.getName());
        tvEmail.setText(patientInfo.getEmail());
        tvAddress.setText(patientInfo.getAddress());
        tvGender.setText("Gender: " + patientInfo.getGender());
        if (!TextUtils.isEmpty(patientInfo.getPhoneNum()))
            tvPhone.setText(patientInfo.getPhoneNum());
        else
            tvPhone.setText("Not Available");


        Questionnaire questionnaire = patientInfo.getQuestionnaire();

        tvAge.setText(getString(R.string.how_old_are_you));
        tvReligious.setText(getString(R.string.do_you_consider_yourself_to_be_religious));
        tvReligion.setText(getString(R.string.what_is_your_religion));
        tvHasTherapist.setText(getString(R.string.have_you_ever_been_in_counseling_or_therapy_before));
        tvSadness.setText(getString(R.string.are_you_currently_experiencing_overwhelming_sadness_grief_or_depression));
        tvSuicidePlan.setText(getString(R.string.when_is_the_last_time_you_had_a_plan_for_suicide));
        tvPanicAttacks.setText(getString(R.string.are_you_currently_experiencing_anxiety_panic_attacks_or_have_any_phobias));
        tvTakingMedications.setText(getString(R.string.are_you_currently_taking_any_medications));
        tvChronicPain.setText(getString(R.string.are_you_currently_experiencing_any_chronic_pain));
        tvFinancialStatus.setText(getString(R.string.how_would_you_rate_your_current_financial_status));
        tvSleepingHabits.setText(getString(R.string.how_would_you_rate_your_current_sleeping_habits));
        tvCountry.setText(getString(R.string.where_are_you_from));
        tvProblems.setText("What problems are you facing? ");


        tvAge.append(" " + questionnaire.getAge() + " years");
        if (questionnaire.isReligious()) {
            tvReligious.append(" Yes");
            tvReligion.append(" " + questionnaire.getReligion());
            tvReligion.setVisibility(View.VISIBLE);
        } else {
            tvReligious.append(" No");
            tvReligion.setVisibility(View.GONE);
        }

        if (questionnaire.isHasTherapist())
            tvHasTherapist.append(" Yes");
        else
            tvHasTherapist.append(" No");

        if (questionnaire.isHasSadnessAndDepression())
            tvSadness.append(" Yes");
        else
            tvSadness.append(" No");

        tvSuicidePlan.append(" " + questionnaire.getSuicidePlanTime());

        if (questionnaire.isHasPanicAttacks())
            tvPanicAttacks.append(" Yes");
        else
            tvPanicAttacks.append(" No");

        if (questionnaire.isTakingMedication())
            tvTakingMedications.append(" Yes");
        else
            tvTakingMedications.append(" No");

        if (questionnaire.isHasChronicPain())
            tvChronicPain.append(" Yes");
        else
            tvChronicPain.append(" No");

        tvFinancialStatus.append(" " + questionnaire.getFinancialStatus());
        tvSleepingHabits.append(" " + questionnaire.getSleepingHabitsStatus());
        tvCountry.append(" " + questionnaire.getCountry());
        tvProblems.append(" " + questionnaire.getProblemsToTreat().substring(0, questionnaire.getProblemsToTreat().length() - 2));
    }

    private void setPictureOnImageView() {
        if (TextUtils.isEmpty(patientInfo.getPicUrl())) {
            ivMyPic.setPadding(0, 0, 0, 0);
            ivMyPic.setImageResource(R.drawable.blank_image);
        } else {
            Glide.with(getActivity()).load(patientInfo.getPicUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .override(300, 200))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            ivMyPic.setPadding(0, 0, 0, 0);
                            ivMyPic.setImageDrawable(resource);
                        }
                    });
        }
    }
}
