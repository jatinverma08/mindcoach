package com.example.rafiquee.mindcoach_adminandtherapist.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.rafiquee.mindcoach_adminandtherapist.fragments.UnderTreatmentPatientsFragment;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.AppointmentsListFragment;


public class HomePageTabsAdapter extends FragmentStatePagerAdapter {


    public HomePageTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new AppointmentsListFragment();
            case 1:
                return new UnderTreatmentPatientsFragment();
        }

        return new AppointmentsListFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }
}
