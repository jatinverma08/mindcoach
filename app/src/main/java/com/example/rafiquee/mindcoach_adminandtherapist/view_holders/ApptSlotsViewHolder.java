package com.example.rafiquee.mindcoach_adminandtherapist.view_holders;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.ApptSlotItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ApptSlotsViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_appt_slot_time)
    public TextView tvApptSlotTime;
    @BindView(R.id.iv_delete)
    public ImageView ivDelete;


    private ArrayList<ApptSlotItem> apptSlotsList;
    private CustomOnClick customOnClick;

    public ApptSlotsViewHolder(final View itemView, ArrayList<ApptSlotItem> apptSlotsList, final CustomOnClick customOnClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.apptSlotsList = apptSlotsList;
        this.customOnClick = customOnClick;

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                customOnClick.onClick(itemView, getAdapterPosition());
            }
        });
    }
}

