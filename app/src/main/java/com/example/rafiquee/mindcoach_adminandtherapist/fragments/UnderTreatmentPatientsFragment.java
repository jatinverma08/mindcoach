package com.example.rafiquee.mindcoach_adminandtherapist.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.adapters.UnderTreatmentPatientsAdapter;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.PatientInfo;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfoORM;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UnderTreatmentPatientsFragment extends BaseFragment {

    @BindView(R.id.rv_my_patients)
    RecyclerView rvMyPatientsList;

    private UnderTreatmentPatientsAdapter underTreatmentPatientsAdapter;
    private ArrayList<PatientInfo> patientsList = new ArrayList<>();
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    private TherapistInfoORM therapistInfoORM;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_under_treatment_patients, container, false);
        ButterKnife.bind(this, view);

        initializations();
        setupListenerForNewPatientAdded();
        return view;
    }

    private void initializations() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        therapistInfoORM = TherapistInfoORM.findById(TherapistInfoORM.class, 1L);
        rvMyPatientsList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void setupListenerForNewPatientAdded() {
        if (isConnectedToInternet()) {
            progressDialog = ProgressDialog.show(getActivity(), "", "Please wait...");
            databaseReference.child("patients_list").orderByChild("therapistId").equalTo(therapistInfoORM.getTherapistId())
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                patientsList.clear();
                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    PatientInfo patientInfo = child.getValue(PatientInfo.class);
                                    patientsList.add(patientInfo);
                                }

                                setupAdapter();
                            }
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (getActivity() != null) {
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else
            Toast.makeText(getActivity(), "Internet connection problem!", Toast.LENGTH_SHORT).show();
    }

    private void setupAdapter() {
        underTreatmentPatientsAdapter = new UnderTreatmentPatientsAdapter(getActivity(), patientsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                replaceFragment(new UnderTreatmentPatientDetailsFragment(patientsList.get(position)), null, true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvMyPatientsList.setAdapter(underTreatmentPatientsAdapter);
    }
}
