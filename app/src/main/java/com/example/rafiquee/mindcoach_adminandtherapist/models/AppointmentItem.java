package com.example.rafiquee.mindcoach_adminandtherapist.models;

import android.os.Parcel;
import android.os.Parcelable;

public class AppointmentItem implements Parcelable {

    private String apptId, duration, status, patientId, patientName, patientPhone, therapistId, therapistName, therapistAddress, therapistPhone;
    private Long date, timeFrom, timeTo;

    public AppointmentItem() {
    }

    protected AppointmentItem(Parcel in) {
        apptId = in.readString();
        duration = in.readString();
        status = in.readString();
        patientId = in.readString();
        patientName = in.readString();
        patientPhone = in.readString();
        therapistId = in.readString();
        therapistName = in.readString();
        therapistAddress = in.readString();
        therapistPhone = in.readString();
        if (in.readByte() == 0) {
            date = null;
        } else {
            date = in.readLong();
        }
        if (in.readByte() == 0) {
            timeFrom = null;
        } else {
            timeFrom = in.readLong();
        }
        if (in.readByte() == 0) {
            timeTo = null;
        } else {
            timeTo = in.readLong();
        }
    }

    public static final Creator<AppointmentItem> CREATOR = new Creator<AppointmentItem>() {
        @Override
        public AppointmentItem createFromParcel(Parcel in) {
            return new AppointmentItem(in);
        }

        @Override
        public AppointmentItem[] newArray(int size) {
            return new AppointmentItem[size];
        }
    };

    public String getApptId() {
        return apptId;
    }

    public void setApptId(String apptId) {
        this.apptId = apptId;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public String getTherapistId() {
        return therapistId;
    }

    public void setTherapistId(String therapistId) {
        this.therapistId = therapistId;
    }

    public String getTherapistName() {
        return therapistName;
    }

    public void setTherapistName(String therapistName) {
        this.therapistName = therapistName;
    }

    public String getTherapistAddress() {
        return therapistAddress;
    }

    public void setTherapistAddress(String therapistAddress) {
        this.therapistAddress = therapistAddress;
    }

    public String getTherapistPhone() {
        return therapistPhone;
    }

    public void setTherapistPhone(String therapistPhone) {
        this.therapistPhone = therapistPhone;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(Long timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Long getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(Long timeTo) {
        this.timeTo = timeTo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(apptId);
        parcel.writeString(duration);
        parcel.writeString(status);
        parcel.writeString(patientId);
        parcel.writeString(patientName);
        parcel.writeString(patientPhone);
        parcel.writeString(therapistId);
        parcel.writeString(therapistName);
        parcel.writeString(therapistAddress);
        parcel.writeString(therapistPhone);
        if (date == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(date);
        }
        if (timeFrom == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(timeFrom);
        }
        if (timeTo == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(timeTo);
        }
    }
}
