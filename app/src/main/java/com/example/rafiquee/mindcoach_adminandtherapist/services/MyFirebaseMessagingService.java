package com.example.rafiquee.mindcoach_adminandtherapist.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.activities.HomeActivity;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfoORM;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by Ehsan Ullah on 1/29/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCM Service";
    private TherapistInfoORM therapistInfoORM;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getData().get("title"));

        therapistInfoORM = TherapistInfoORM.findById(TherapistInfoORM.class, 1L);
        sendBigTextStyleNotification(remoteMessage.getData());
    }

    public void sendBigTextStyleNotification(Map<String, String> data) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, HomeActivity.class), 0);
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle("Dear " + therapistInfoORM.getName() + "\n" + data.get("title"))
                .setContentText(data.get("body"))
                .setSmallIcon(R.drawable.logo_blue_square)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .addAction(R.mipmap.ic_launcher, "Open", pi);

        //______________ viberation _______________
        builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});

        //______________ ring tune _______________
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);

        //______________ remove from status bar after clicked ____________
        builder.setAutoCancel(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            builder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        }

        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification = new Notification.BigTextStyle(builder)
                    .bigText(data.get("body")).build();
        }

        notificationManager.notify(1, notification);
    }
}
