package com.example.rafiquee.mindcoach_adminandtherapist.models;

import com.google.gson.annotations.SerializedName;

public class HelpRequest {

    @SerializedName("to")
    private String token;

    @SerializedName("notification")
    private HelpRequestModel helpRequestModel;

    @SerializedName("data")
    private HelpRequestDataModel helpRequestDataModel;

    public HelpRequestModel getHelpRequestModel() {
        return helpRequestModel;
    }

    public void setHelpRequestModel(HelpRequestModel helpRequestModel) {
        this.helpRequestModel = helpRequestModel;
    }

    public HelpRequestDataModel getHelpRequestDataModel() {
        return helpRequestDataModel;
    }

    public void setHelpRequestDataModel(HelpRequestDataModel helpRequestDataModel) {
        this.helpRequestDataModel = helpRequestDataModel;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
