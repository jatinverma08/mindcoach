package com.example.rafiquee.mindcoach_adminandtherapist.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.AppointmentItem;
import com.example.rafiquee.mindcoach_adminandtherapist.utils.ConstantIds;
import com.example.rafiquee.mindcoach_adminandtherapist.view_holders.AppointmentsListViewHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class AppointmentsListAdapter extends RecyclerView.Adapter<AppointmentsListViewHolder> {

    private CustomOnClick customOnClick;
    private ArrayList<AppointmentItem> appointmentsList;
    private Context context;

    public AppointmentsListAdapter() {
    }

    public AppointmentsListAdapter(Context context, ArrayList<AppointmentItem> appointmentsList, CustomOnClick customOnClick) {
        this.context = context;
        this.appointmentsList = appointmentsList;
        this.customOnClick = customOnClick;
    }

    @Override
    public AppointmentsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_appoinment_list_item, parent, false);

        AppointmentsListViewHolder vh = new AppointmentsListViewHolder(view, appointmentsList, customOnClick);
        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(AppointmentsListViewHolder holder, int position) {
        holder.tvName.setText(appointmentsList.get(position).getPatientName());
        if (!TextUtils.isEmpty(appointmentsList.get(position).getPatientPhone()))
            holder.tvPhone.setText(appointmentsList.get(position).getPatientPhone());
        holder.tvApptDate.setText(getFormattedDate(appointmentsList.get(position).getDate()));
        holder.tvApptDuration.setText("Duration: " + getFormattedTime(appointmentsList.get(position).getTimeFrom())
                + " - " + getFormattedTime(appointmentsList.get(position).getTimeTo()));
        holder.tvAddress.setText(appointmentsList.get(position).getTherapistAddress());
        holder.tvStatus.setText(appointmentsList.get(position).getStatus());

        switch (appointmentsList.get(position).getStatus()) {

            case ConstantIds.PENDING:
                holder.tvStatus.setTextColor(Color.BLUE);
                break;

            case ConstantIds.CANCELLED:
                holder.tvStatus.setTextColor(Color.RED);
                break;

            case ConstantIds.EXPIRED:
                holder.tvStatus.setTextColor(Color.DKGRAY);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return appointmentsList.size();
    }

    private String getFormattedTime(long time) {
        Date timeFromDate = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private String getFormattedDate(long time) {
        Date timeFromDate = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }
}