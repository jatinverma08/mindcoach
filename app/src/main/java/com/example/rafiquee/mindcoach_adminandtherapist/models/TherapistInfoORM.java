package com.example.rafiquee.mindcoach_adminandtherapist.models;

import com.orm.SugarRecord;

import java.util.ArrayList;

public class TherapistInfoORM extends SugarRecord {

    private String therapistId, name, email, phoneNum, address, password, picUrl, gender, degrees;
    private long availableTimeFrom, availableTimeTo;
    private ArrayList<String> availableDays = new ArrayList<>();
    private ArrayList<String> specialitiesList = new ArrayList<>();
    private ArrayList<String> patientsList = new ArrayList<>();


    public TherapistInfoORM() {
    }

    public String getTherapistId() {
        return therapistId;
    }

    public void setTherapistId(String therapistId) {
        this.therapistId = therapistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDegrees() {
        return degrees;
    }

    public void setDegrees(String degrees) {
        this.degrees = degrees;
    }

    public long getAvailableTimeFrom() {
        return availableTimeFrom;
    }

    public void setAvailableTimeFrom(long availableTimeFrom) {
        this.availableTimeFrom = availableTimeFrom;
    }

    public long getAvailableTimeTo() {
        return availableTimeTo;
    }

    public void setAvailableTimeTo(long availableTimeTo) {
        this.availableTimeTo = availableTimeTo;
    }

    public ArrayList<String> getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(ArrayList<String> availableDays) {
        this.availableDays = availableDays;
    }

    public ArrayList<String> getSpecialitiesList() {
        return specialitiesList;
    }

    public void setSpecialitiesList(ArrayList<String> specialitiesList) {
        this.specialitiesList = specialitiesList;
    }

    public ArrayList<String> getPatientsList() {
        return patientsList;
    }

    public void setPatientsList(ArrayList<String> patientsList) {
        this.patientsList = patientsList;
    }
}
