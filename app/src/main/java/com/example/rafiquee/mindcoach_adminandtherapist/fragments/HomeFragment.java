package com.example.rafiquee.mindcoach_adminandtherapist.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.adapters.HomePageTabsAdapter;
import com.example.rafiquee.mindcoach_adminandtherapist.fragments.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    private HomePageTabsAdapter homePageTabsAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        initializations();
        return view;
    }

    private void initializations() {
        homePageTabsAdapter = new HomePageTabsAdapter(getChildFragmentManager());
        viewPager.setAdapter(homePageTabsAdapter);
        tabLayout.setupWithViewPager(viewPager);
        setUpTabs();
    }

    private void setUpTabs() {

        TextView tab1 = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.layout_tab_text, null);
        tab1.setText("Appointments");
        tabLayout.getTabAt(0).setCustomView(tab1);

        TextView tab2 = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.layout_tab_text, null);
        tab2.setText("Patients");
        tabLayout.getTabAt(1).setCustomView(tab2);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrolled(int position, float offset, int offsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
}
