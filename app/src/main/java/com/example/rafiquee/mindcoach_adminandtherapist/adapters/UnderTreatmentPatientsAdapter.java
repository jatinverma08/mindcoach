package com.example.rafiquee.mindcoach_adminandtherapist.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.PatientInfo;
import com.example.rafiquee.mindcoach_adminandtherapist.view_holders.UnderTreatmentPatientsViewHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by khalidz on 9/5/2017.
 */

public class UnderTreatmentPatientsAdapter extends RecyclerView.Adapter<UnderTreatmentPatientsViewHolder> {

    private Context context;
    private CustomOnClick customOnClick;
    private ArrayList<PatientInfo> patientsList;

    public UnderTreatmentPatientsAdapter() {
    }

    public UnderTreatmentPatientsAdapter(Context context, ArrayList<PatientInfo> patientsList, CustomOnClick customOnClick) {
        this.context = context;
        this.patientsList = patientsList;
        this.customOnClick = customOnClick;
    }

    @Override
    public UnderTreatmentPatientsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_patient_list_item, parent, false);

        UnderTreatmentPatientsViewHolder vh = new UnderTreatmentPatientsViewHolder(view, patientsList, customOnClick);
        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(UnderTreatmentPatientsViewHolder holder, int position) {
        setPictureOnTherapistItem(position, holder);
        holder.tvName.setText(patientsList.get(position).getName());
        holder.tvPhoneNum.setText("Phone: " + patientsList.get(position).getPhoneNum());
        holder.tvCountry.setText("Country: " + patientsList.get(position).getCountry());

        if (TextUtils.isEmpty(patientsList.get(position).getTherapistId()))
            holder.tvUnderTreatment.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.checkbox_off_background, 0, 0, 0);
        else
            holder.tvUnderTreatment.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.checkbox_on_background, 0, 0, 0);
    }

    @Override
    public int getItemCount() {
        return patientsList.size();
    }

    private String getFormattedTime(long timeFrom) {
        Date timeFromDate = new Date(timeFrom);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }

    private void setPictureOnTherapistItem(int position, final UnderTreatmentPatientsViewHolder underTreatmentPatientsViewHolder) {
        if (TextUtils.isEmpty(patientsList.get(position).getPicUrl())) {
            underTreatmentPatientsViewHolder.ivPatientPic.setPadding(0, 0, 0, 0);
            underTreatmentPatientsViewHolder.ivPatientPic.setImageResource(R.drawable.blank_image);
        } else {
            Glide.with(context).load(patientsList.get(position).getPicUrl())
                    .apply(new RequestOptions()
                            .centerCrop()
                            .override(300, 200))
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
                            underTreatmentPatientsViewHolder.ivPatientPic.setPadding(0, 0, 0, 0);
                            underTreatmentPatientsViewHolder.ivPatientPic.setImageDrawable(resource);
                        }
                    });
        }
    }
}