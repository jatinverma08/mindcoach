package com.example.rafiquee.mindcoach_adminandtherapist.view_holders;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.PatientInfo;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TherapistListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_therapist_pic)
    public ImageView ivTherapistPic;
    @BindView(R.id.tv_name)
    public TextView tvName;
    @BindView(R.id.tv_degrees)
    public TextView tvDegrees;
    @BindView(R.id.tv_phone_num)
    public TextView tvPhoneNum;
    @BindView(R.id.tv_available_timing)
    public TextView tvAvailableTiming;
    @BindView(R.id.tv_num_of_patients)
    public TextView tvNumOfPatients;

    private ArrayList<TherapistInfo> therapistsList;
    private CustomOnClick customOnClick;

    public TherapistListViewHolder(final View itemView, ArrayList<TherapistInfo> therapistsList, final CustomOnClick customOnClick) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.therapistsList = therapistsList;
        this.customOnClick = customOnClick;

        itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                customOnClick.onClick(itemView, getAdapterPosition());
            }
        });
    }
}

