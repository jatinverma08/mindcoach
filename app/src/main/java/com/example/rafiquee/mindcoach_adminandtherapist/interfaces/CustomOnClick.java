package com.example.rafiquee.mindcoach_adminandtherapist.interfaces;

import android.view.View;

public interface CustomOnClick {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
