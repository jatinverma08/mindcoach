package com.example.rafiquee.mindcoach_adminandtherapist.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.activities.AppointmentDetailsActivity;
import com.example.rafiquee.mindcoach_adminandtherapist.adapters.AppointmentsListAdapter;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.AppointmentItem;
import com.example.rafiquee.mindcoach_adminandtherapist.models.TherapistInfoORM;
import com.example.rafiquee.mindcoach_adminandtherapist.utils.ConstantIds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppointmentsListFragment extends BaseFragment {


    @BindView(R.id.rv_appt_list)
    RecyclerView rvAppointmentsList;

    private DatabaseReference databaseReference;
    private AppointmentsListAdapter appointmentsListAdapter;
    private ArrayList<AppointmentItem> appointmentsList = new ArrayList<>();
    private TherapistInfoORM therapistInfoORM;
    public static boolean APPT_STATUS_UPDATED = false;
    private int selectedAppointmentIndex;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getActivity().setTitle("Appointments History");
                }
            }, 5);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appointments_list, container, false);
        ButterKnife.bind(this, view);

        initializations(view);
        fetchMyAppointmentFromFirebase();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (APPT_STATUS_UPDATED) {
            APPT_STATUS_UPDATED = false;
            appointmentsList.get(selectedAppointmentIndex).setStatus(ConstantIds.CANCELLED);
            appointmentsListAdapter.notifyItemChanged(selectedAppointmentIndex);
        }
    }

    private void initializations(View view) {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        therapistInfoORM = TherapistInfoORM.findById(TherapistInfoORM.class, 1L);

        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvAppointmentsList.setLayoutManager(linearLayoutManager);
    }

    private void fetchMyAppointmentFromFirebase() {
        if (isConnectedToInternet()) {
            databaseReference.child("appointments_list").orderByChild("therapistId").equalTo(therapistInfoORM.getTherapistId())
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                appointmentsList.clear();
                                for (DataSnapshot data : dataSnapshot.getChildren())
                                    appointmentsList.add(data.getValue(AppointmentItem.class));

                                setupAdapter();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if (getActivity() != null) {
                                Toast.makeText(getActivity(), databaseError.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        } else
            Toast.makeText(getActivity(), "Internet connection problem!", Toast.LENGTH_SHORT).show();

    }

    private void setupAdapter() {
        Collections.reverse(appointmentsList);
        appointmentsListAdapter = new AppointmentsListAdapter(getActivity(), appointmentsList, new CustomOnClick() {
            @Override
            public void onClick(View view, int position) {
                selectedAppointmentIndex = position;

                Intent intent = new Intent(getActivity(), AppointmentDetailsActivity.class);
                intent.putExtra("appointment_item", appointmentsList.get(position));
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });

        rvAppointmentsList.setAdapter(appointmentsListAdapter);
    }
}
