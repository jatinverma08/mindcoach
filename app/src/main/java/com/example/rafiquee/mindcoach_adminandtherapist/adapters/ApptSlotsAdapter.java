package com.example.rafiquee.mindcoach_adminandtherapist.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rafiquee.mindcoach_adminandtherapist.R;
import com.example.rafiquee.mindcoach_adminandtherapist.interfaces.CustomOnClick;
import com.example.rafiquee.mindcoach_adminandtherapist.models.ApptSlotItem;
import com.example.rafiquee.mindcoach_adminandtherapist.view_holders.ApptSlotsViewHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ApptSlotsAdapter extends RecyclerView.Adapter<ApptSlotsViewHolder> {

    private Context context;
    private CustomOnClick customOnClick;
    private ArrayList<ApptSlotItem> apptSlotsList;

    public ApptSlotsAdapter() {
    }

    public ApptSlotsAdapter(Context context, ArrayList<ApptSlotItem> apptSlotsList, CustomOnClick customOnClick) {
        this.context = context;
        this.apptSlotsList = apptSlotsList;
        this.customOnClick = customOnClick;
    }

    @Override
    public ApptSlotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_appt_slot_item, parent, false);

        ApptSlotsViewHolder vh = new ApptSlotsViewHolder(view, apptSlotsList, customOnClick);
        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ApptSlotsViewHolder holder, int position) {
        holder.tvApptSlotTime.setText(getFormattedTime(apptSlotsList.get(position).getTimeFrom()) + " - " +
                getFormattedTime(apptSlotsList.get(position).getTimeTo()));
    }

    @Override
    public int getItemCount() {
        return apptSlotsList.size();
    }

    private String getFormattedTime(long timeFrom) {
        Date timeFromDate = new Date(timeFrom);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        return simpleDateFormat.format(timeFromDate);
    }
}